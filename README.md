# Tiny Test - Comic Strips App

Production front-end environment - https://tiny-test-comics-xkcd.herokuapp.com/

## Technologies used

- React.js (For front-end)
- Typescript (For typesafe code on front-end)
- Firebase (For Authentication)
- Firebase Firestore (For Data storage)
- NodeJs and Express (For proxy server)
- XKCD API (For fetching comics)
- TailwindCSS (For styling)

## How to run project locally

- git clone project
- run npm install
- run npm start
- the back-end directory is on another git repository which has the Express server. However the proxy server is being loaded in production on Heroku