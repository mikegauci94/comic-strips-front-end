import { useState, useEffect } from "react";

// Services
import { getComicByNumber, getLastComicNumber } from "services/comics";

// Utils
import { generateRandomNumber } from "utils/functions";

// Router
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

// Components
import { ComicHome, ComicDetails, SignIn, SignUp, Layout } from "components";

// Types
import { RandomComicType } from "types";

function App() {
  const [loading, setLoading] = useState<boolean>(false);
  const [lastComicNumber, setLastComicNumber] = useState<number>(2000);
  const [randomComics, setRandomComics] = useState<RandomComicType[] | []>([]);
  const [reload, setReload] = useState<boolean>(false);
  const [render, setRender] = useState<boolean>(false);

  const mapper = [1, 2, 3, 4, 5, 6, 7, 8, 9];

  // Functions
  const getLastComic = async () => {
    const lastComic = await getLastComicNumber();

    if (lastComic?.num) {
      setLastComicNumber(lastComic?.num);
    } else {
      console.log("Error", lastComic);
    }
  };

  const getComic = async (item: any) => {
    return getComicByNumber(item);
  };

  const getComics = async () => {
    return Promise.all(
      mapper.map(() => getComic(generateRandomNumber(lastComicNumber)))
    );
  };

  useEffect(() => {
    setLoading(true);
    getComics().then((comics: any) => {
      setRandomComics(comics);
      setLoading(false);
    });

    getLastComic();
  }, [reload]);

  return (
    <Router>
      <Layout render={render} setRender={setRender}>
        <Routes>
          <Route
            path="/"
            element={
              <ComicHome
                loading={loading}
                randomComics={randomComics}
                reload={reload}
                setReload={setReload}
                render={render}
                setRender={setRender}
                setRandomComics={setRandomComics}
              />
            }
          />
          <Route
            path="/comics/:id"
            element={
              <ComicDetails
                randomComics={randomComics}
                render={render}
                setRender={setRender}
                setRandomComics={setRandomComics}
              />
            }
          />
          <Route
            path="/signin"
            element={<SignIn render={render} setRender={setRender} />}
          />
          <Route
            path="/signup"
            element={<SignUp render={render} setRender={setRender} />}
          />
        </Routes>
      </Layout>
    </Router>
  );
}

export default App;
