import React, { Dispatch, SetStateAction, useState } from "react";

// Firestore
import { db, doc, setDoc, getDoc } from "config/firebaseConfig";

// Router
import { Link, useNavigate } from "react-router-dom";

// Types
import { RandomComicType } from "types";

interface ComicHomePropsType {
  loading: boolean;
  randomComics: RandomComicType[];
  setRandomComics: Dispatch<SetStateAction<RandomComicType[]>>;
  setReload: Dispatch<SetStateAction<boolean>>;
  reload: boolean;
  setRender: Dispatch<SetStateAction<boolean>>;
  render: boolean;
}

function classNames(...classes: any) {
  return classes.filter(Boolean).join(" ");
}

export const ComicHome: React.FC<ComicHomePropsType> = ({
  loading,
  randomComics,
  setReload,
  reload,
  render,
  setRandomComics,
  setRender,
}) => {
  const navigate = useNavigate();

  const [upvoting, setUpvoting] = useState({ loading: false, id: "" });
  const [downvoting, setDownvoting] = useState({ loading: false, id: "" });

  // Handlers
  const handleUpvote = async (num: string) => {
    const docRef = doc(db, "votes", num.toString());
    setUpvoting({ loading: true, id: num });
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
      // Check if already upvoted
      const alreadyUpvoted = docSnap
        .data()
        .upvoters.find(
          (upvoter: string) => upvoter === localStorage.getItem("user")
        );

      // Check if downvoted
      const alreadyDownvoted = docSnap
        .data()
        .downvoters.find(
          (downvoter: string) => downvoter === localStorage.getItem("user")
        );

      // If Already upvoted
      if (alreadyUpvoted) {
        // * Remove from upvoters list
        let filteredUpvoters = docSnap
          .data()
          .upvoters.filter(
            (upvoter: string) => upvoter !== localStorage.getItem("user")
          );

        setDoc(doc(db, "votes", num), {
          ...docSnap.data(),
          upvoters: filteredUpvoters,
        })
          .then(() => {
            let tempComics = randomComics.filter(
              (randomComic) => randomComic?.num !== Number(num)
            );
            let tempComic = randomComics.filter(
              (randomComic) => randomComic?.num === Number(num)
            );

            setRandomComics([
              {
                ...tempComic[0],
                upvoters: filteredUpvoters,
              },
              ...tempComics,
            ]);
            setRender(!render);

            setUpvoting({ loading: false, id: "" });
          })
          .catch((err) => {
            console.error("Error removing upvote !", err);
          });
        // * Remove from upvoters list
      } else if (alreadyDownvoted) {
        // Remove from downvoters list
        let filteredDownvoters = docSnap
          .data()
          .downvoters.filter(
            (downvoter: string) => downvoter !== localStorage.getItem("user")
          );

        setDoc(doc(db, "votes", num), {
          ...docSnap.data(),
          downvoters: filteredDownvoters,
        }).then(() => {
          let tempComics = randomComics.filter(
            (randomComic) => randomComic?.num !== Number(num)
          );
          let tempComic = randomComics.filter(
            (randomComic) => randomComic?.num === Number(num)
          );

          // * Add to upvoters list
          setDoc(doc(db, "votes", num), {
            downvoters: filteredDownvoters,
            upvoters: [
              ...docSnap.data().upvoters,
              localStorage.getItem("user"),
            ],
          })
            .then(() => {
              setRandomComics([
                {
                  ...tempComic[0],
                  downvoters: filteredDownvoters,
                  upvoters: [
                    ...docSnap.data().upvoters,
                    localStorage.getItem("user"),
                  ],
                },
                ...tempComics,
              ]);
              // * Add to downvoters list

              setRender(!render);
              setUpvoting({ loading: false, id: "" });
            })
            .catch((err) => {
              console.error("Error removing upvote !", err);
            });
        });

        // Add to downvoters list
      } else {
        // Add to upvoters list
        setDoc(doc(db, "votes", num), {
          ...docSnap.data(),
          upvoters: [...docSnap.data().upvoters, localStorage.getItem("user")],
        })
          .then(() => {
            let tempComics = randomComics.filter(
              (randomComic) => randomComic?.num !== Number(num)
            );
            let tempComic = randomComics.filter(
              (randomComic) => randomComic?.num === Number(num)
            );

            setRandomComics([
              {
                ...tempComic[0],
                upvoters: [
                  ...docSnap.data().upvoters,
                  localStorage.getItem("user"),
                ],
              },
              ...tempComics,
            ]);
            setRender(!render);
            // Add to upvoters list

            setUpvoting({ loading: false, id: "" });
          })
          .catch((err) => {
            console.error("Error upvoting !", err);
          });
      }
    }
  };

  const handleDownvote = async (num: string) => {
    const docRef = doc(db, "votes", num.toString());
    setDownvoting({ loading: true, id: num });
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
      // Check if already upvoted
      const alreadyUpvoted = docSnap
        .data()
        .upvoters.find(
          (upvoter: string) => upvoter === localStorage.getItem("user")
        );

      // Check if downvoted
      const alreadyDownvoted = docSnap
        .data()
        .downvoters.find(
          (downvoter: string) => downvoter === localStorage.getItem("user")
        );

      // If Already upvoted
      if (alreadyUpvoted) {
        // Remove from upvoters list
        let filteredUpvoters = docSnap
          .data()
          .upvoters.filter(
            (upvoter: string) => upvoter !== localStorage.getItem("user")
          );

        setDoc(doc(db, "votes", num), {
          ...docSnap.data(),
          upvoters: filteredUpvoters,
        }).then(() => {
          let tempComics = randomComics.filter(
            (randomComic) => randomComic?.num !== Number(num)
          );
          let tempComic = randomComics.filter(
            (randomComic) => randomComic?.num === Number(num)
          );

          // * Add to downvoters list
          setDoc(doc(db, "votes", num), {
            upvoters: filteredUpvoters,
            downvoters: [
              ...docSnap.data().downvoters,
              localStorage.getItem("user"),
            ],
          })
            .then(() => {
              setRandomComics([
                {
                  ...tempComic[0],
                  upvoters: filteredUpvoters,
                  downvoters: [
                    ...docSnap.data().downvoters,
                    localStorage.getItem("user"),
                  ],
                },
                ...tempComics,
              ]);
              // * Add to downvoters list

              setRender(!render);
              setDownvoting({ loading: false, id: "" });
            })
            .catch((err) => {
              console.error("Error removing upvote !", err);
            });
        });

        // Add to downvoters list
      } else if (alreadyDownvoted) {
        // * Remove from downvoters list
        let filtereDownvoters = docSnap
          .data()
          .downvoters.filter(
            (downvoter: string) => downvoter !== localStorage.getItem("user")
          );

        setDoc(doc(db, "votes", num), {
          ...docSnap.data(),
          downvoters: filtereDownvoters,
        })
          .then(() => {
            let tempComics = randomComics.filter(
              (randomComic) => randomComic?.num !== Number(num)
            );
            let tempComic = randomComics.filter(
              (randomComic) => randomComic?.num === Number(num)
            );

            setRandomComics([
              {
                ...tempComic[0],
                downvoters: filtereDownvoters,
              },
              ...tempComics,
            ]);
            setRender(!render);

            setDownvoting({ loading: false, id: "" });
          })
          .catch((err) => {
            console.error("Error removing upvote !", err);
          });
        // * Remove from downvoters list
      } else {
        // Add to downvoters list
        setDoc(doc(db, "votes", num), {
          ...docSnap.data(),
          downvoters: [
            ...docSnap.data().downvoters,
            localStorage.getItem("user"),
          ],
        })
          .then(() => {
            let tempComics = randomComics.filter(
              (randomComic) => randomComic?.num !== Number(num)
            );
            let tempComic = randomComics.filter(
              (randomComic) => randomComic?.num === Number(num)
            );

            setRandomComics([
              {
                ...tempComic[0],
                downvoters: [
                  ...docSnap.data().downvoters,
                  localStorage.getItem("user"),
                ],
              },
              ...tempComics,
            ]);
            setRender(!render);
            // Add to downvoters list

            setDownvoting({ loading: false, id: "" });
          })
          .catch((err) => {
            console.error("Error upvoting !", err);
          });
      }
    }
  };

  const handleRedirect = () => {
    navigate(`/signin`);
  };

  return (
    <div className="bg-white lg:flex lg:flex-col lg:justify-center lg:items-center lg:mt-10">
      <div className="max-w-2xl mx-auto py-10 px-4 sm:py-4 sm:px-6 lg:max-w-7xl lg:px-8">
        <h2 id="products-heading" className="text-3xl text-center mb-4">
          Comic Strips
        </h2>

        {loading ? (
          <div className="flex flex-col justify-center items-center min-h-[600px]">
            <svg
              role="status"
              className="mr-2 w-16 md:w-20 text-gray-200 animate-spin dark:text-gray-600 fill-gray-700"
              viewBox="0 0 100 101"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                fill="currentColor"
              />
              <path
                d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                fill="currentFill"
              />
            </svg>
          </div>
        ) : (
          <div className="grid max-w-[800px] mx-auto grid-cols-1 gap-y-4 sm:grid-cols-2 gap-x-6 lg:grid-cols-3 xl:gap-x-4">
            {randomComics
              ?.sort((a, b) => a.num - b.num)
              ?.map(({ img, num, alt, downvoters, upvoters }) => (
                <div
                  key={num}
                  className="relative drop-shadow-md lg:w-64 border border-gray-400"
                >
                  <Link
                    to={`/comics/${num}`}
                    className="block w-full min-h-32 bg-gray-200 aspect-w-1 aspect-h-1 rounded-md overflow-hidden hover:opacity-75 lg:h-48  lg:aspect-none"
                  >
                    <img
                      src={img}
                      alt={alt}
                      className="w-full h-full object-center object-cover lg:w-full lg:h-full"
                    />
                  </Link>
                  <div className="flex items-center justify-center top-2 left-2 bg-gray-100 px-2 py-2 text-black">
                    {upvoting?.loading && upvoting?.id === num.toString() ? (
                      <svg
                        role="status"
                        className="w-4 text-gray-200 animate-spin dark:text-gray-600 fill-gray-700"
                        viewBox="0 0 100 101"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                          fill="currentColor"
                        />
                        <path
                          d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                          fill="currentFill"
                        />
                      </svg>
                    ) : (
                      <div
                        onClick={() => {
                          localStorage.getItem("user")
                            ? handleUpvote(num.toString())
                            : handleRedirect();
                        }}
                        className={classNames(
                          upvoters.find(
                            (upvoter: string) =>
                              upvoter === localStorage.getItem("user")
                          )
                            ? "bg-green-600 hover:bg-white"
                            : " hover:bg-green-600",
                          `group cursor-pointer border border-green-600 rounded-md flex justify-center items-center p-1 transition-all duration-150 ease-in`
                        )}
                      >
                        <svg
                          data-name="1-Arrow Up"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 32 32"
                          className={classNames(
                            upvoters.find(
                              (upvoter: string) =>
                                upvoter === localStorage.getItem("user")
                            )
                              ? "fill-white group-hover:fill-green-600"
                              : "fill-green-600 group-hover:fill-white",
                            ` w-3 transition-all duration-200 ease-in-out`
                          )}
                        >
                          <path d="m26.71 10.29-10-10a1 1 0 0 0-1.41 0l-10 10 1.41 1.41L15 3.41V32h2V3.41l8.29 8.29z" />
                        </svg>
                      </div>
                    )}
                    <span className="mx-2 font-medium text-base pointer-events-none">
                      {upvoters.length - downvoters.length}
                    </span>
                    {downvoting?.loading &&
                    downvoting?.id === num.toString() ? (
                      <svg
                        role="status"
                        className="w-4 text-gray-200 animate-spin dark:text-gray-600 fill-gray-700"
                        viewBox="0 0 100 101"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                          fill="currentColor"
                        />
                        <path
                          d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                          fill="currentFill"
                        />
                      </svg>
                    ) : (
                      <div
                        onClick={() => {
                          localStorage.getItem("user")
                            ? handleDownvote(num.toString())
                            : handleRedirect();
                        }}
                        className={classNames(
                          downvoters.find(
                            (downvoter: string) =>
                              downvoter === localStorage.getItem("user")
                          )
                            ? "bg-red-600 hover:bg-white"
                            : " hover:bg-red-600",
                          `group cursor-pointer border border-red-600 rounded-md flex justify-center items-center p-1 transition-all duration-150 ease-in`
                        )}
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 25 25"
                          className={classNames(
                            downvoters.find(
                              (downvoter: string) =>
                                downvoter === localStorage.getItem("user")
                            )
                              ? "fill-white group-hover:fill-red-600"
                              : "fill-red-600 group-hover:fill-white",
                            ` w-3 transition-all duration-200 ease-in-out`
                          )}
                        >
                          <path d="m18.294 16.793-5.293 5.293V1h-1v21.086l-5.295-5.294-.707.707L12.501 24l6.5-6.5-.707-.707z" />
                        </svg>
                      </div>
                    )}
                  </div>
                </div>
              ))}
          </div>
        )}

        <div className="w-full flex items-center justify-center my-3">
          <button
            onClick={() => {
              setReload(!reload);
            }}
            className="bg-gray-800 text-white border border-transparent hover:bg-white hover:text-gray-800 hover:border-gray-800 transition-all duration-300 font-medium px-16 py-2 rounded-md"
          >
            Reload
          </button>
        </div>
      </div>
    </div>
  );
};
