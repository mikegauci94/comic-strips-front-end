import {
  ChangeEvent,
  Dispatch,
  FormEvent,
  SetStateAction,
  useState,
  useEffect,
} from "react";

// Services
import { signInUser } from "services/auth";

// Router
import { Link, useNavigate } from "react-router-dom";

// Types
import { SignInDataType } from "types";

interface SignInPropType {
  setRender: Dispatch<SetStateAction<boolean>>;
  render: boolean;
}

interface SignInErrors {
  email: string;
  password: string;
  other: string;
}

function classNames(...classes: any) {
  return classes.filter(Boolean).join(" ");
}

export const SignIn: React.FC<SignInPropType> = ({ render, setRender }) => {
  const navigate = useNavigate();

  const [loading, setLoading] = useState<boolean>(false);

  const [errors, setErrors] = useState<SignInErrors>({
    email: "",
    password: "",
    other: "",
  });

  const [signInData, setSignInData] = useState<SignInDataType>({
    email: "",
    password: "",
  });

  // Handlers
  const clearForm = () => {
    setSignInData({
      email: "",
      password: "",
    });
  };

  const clearErrors = () => {
    setErrors({
      email: "",
      password: "",
      other: "",
    });
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setSignInData({ ...signInData, [event.target.name]: event.target.value });
    setErrors({ ...errors, [event.target.name]: "", other: "" });
  };

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    console.log({ signInData });

    setLoading(true);
    signInUser(signInData.email, signInData.password).then((resp) => {
      if (resp?.uid) {
        setRender(!render);
        clearErrors();
        clearForm();
        navigate(`/`);
      } else if (resp.code === "auth/email-already-exists") {
        console.log("Email Error: ", resp.message);
        setErrors({ ...errors, email: "Email already exists" });
      } else if (resp.code === "auth/invalid-email") {
        console.log("Email Error: ", resp.message);
        setErrors({ ...errors, email: "Invalid email" });
      } else if (resp.code === "auth/wrong-password") {
        console.log("Password Error: ", resp.message);
        setErrors({ ...errors, password: "Wrong password" });
      } else if (resp.code === "auth/user-not-found") {
        console.log("User Error: ", resp.message);
        setErrors({ ...errors, other: "User not found" });
      } else if (resp.code === "auth/internal-error") {
        console.log("Server Error: ", resp.message);
        setErrors({ ...errors, other: "Internal server error" });
      } else {
        console.log("Other error: ", resp.message);
        setErrors({ ...errors, other: resp.code });
      }

      setLoading(false);
    });
  };

  useEffect(() => {
    if (localStorage.getItem("user")) {
      navigate(`/`);
    }
  }, []);

  return (
    <div className="min-h-full flex flex-col justify-center py-12 sm:px-6 lg:px-8">
      <div className="sm:mx-auto sm:w-full sm:max-w-md  flex flex-col">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 98 32"
          className="w-auto h-7 fill-indigo-600"
        >
          <g>
            <title>Tiny Logo</title>
            <path d="M84.831 7.352l3.216 9.055.766 2.762.918-2.762 3.216-9.055H98l-8.437 23.574L84.112 32l2.25-6.385-6.584-18.263h5.053zM20.24 0c5.206.03 10.35 4.357 10.35 10.662 0 0 .032 1.602.036 3.54v.81c-.001.365-.004.735-.007 1.103l-.007.55c-.023 1.548-.077 2.989-.19 3.739-.72 4.817-4.318 8.146-9.278 8.99-4.47.874-7.12 1.38-7.977 1.549-.367.077-1.99.291-2.694.291-5.45 0-10.395-4.08-10.472-10.662v-1.081l.001-.186v-.406c.002-.709.004-1.578.008-2.472l.003-.539c.008-1.709.023-3.414.05-4.198.183-4.802 3.505-8.683 9.828-9.926L17.943.2C18.678.061 19.489 0 20.239 0zm25.392 2.44v4.912h4.594v4.45h-4.594v7.674c0 1.504 1.133 2.149 1.838 2.149.603 0 1.151-.068 1.71-.222l.28-.085 1.072 3.53c-.46.307-1.837 1.074-4.44 1.074-2.604 0-5.299-1.995-5.36-5.371-.043-2.124-.046-4.832-.007-8.124l.007-.625h-3.215v-4.45h3.215v-3.96l4.9-.952zM23.883 5.6L11.635 7.976v4.787l-4.899.95V25.65l12.248-2.378v-4.786l4.899-.951V5.599zm47.78 1.291c4.265-.03 7.499 3.335 7.65 7.577l.006.25v10.897h-4.9v-10.13c-.016-2.301-1.685-4.158-3.981-4.143-2.228.015-4.181 1.733-4.284 3.936l-.004.208v10.13h-4.9V7.351h4.44l.17 2.133c1.423-1.566 3.52-2.579 5.802-2.594zm-13.475.46v18.264h-4.9V7.352h4.9zm-39.204 3.986v7.149l-7.349 1.427v-7.15l7.349-1.426zM58.187 0v4.773l-4.9.952V.952l4.9-.952z"></path>
          </g>
        </svg>
        <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
          Sign in to your account
        </h2>
      </div>

      <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
        <div className="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
          <form className="space-y-6" onSubmit={handleSubmit}>
            <div>
              <label
                htmlFor="email"
                className="block text-sm font-medium text-gray-700"
              >
                Email address
              </label>
              <div className="mt-1">
                <input
                  id="email"
                  name="email"
                  type="email"
                  autoComplete="email"
                  required
                  value={signInData.email}
                  onChange={handleChange}
                  className={classNames(
                    errors.email !== "" ? "border-red-500" : "border-gray-300",
                    `appearance-none block w-full px-3 py-2 border rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-gray-500 focus:border-gray-500 sm:text-sm`
                  )}
                />
              </div>
              {errors.email !== "" && (
                <span className="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                  {errors.email}
                </span>
              )}
            </div>

            <div>
              <label
                htmlFor="password"
                className="block text-sm font-medium text-gray-700"
              >
                Password
              </label>
              <div className="mt-1">
                <input
                  id="password"
                  name="password"
                  type="password"
                  autoComplete="current-password"
                  required
                  value={signInData.password}
                  onChange={handleChange}
                  className={classNames(
                    errors.password !== ""
                      ? "border-red-500"
                      : "border-gray-300",
                    `appearance-none block w-full px-3 py-2 border rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-gray-500 focus:border-gray-500 sm:text-sm`
                  )}
                />
              </div>
              {errors.password !== "" && (
                <span className="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                  {errors.password}
                </span>
              )}
            </div>

            <div>
              {errors.other !== "" && (
                <span className="flex items-center font-medium justify-center tracking-wide text-red-500 text-xs ml-1 mb-2">
                  {errors.other}
                </span>
              )}
              <button
                type="submit"
                className="flex items-center justify-center bg-gray-800 text-white border border-transparent hover:bg-white hover:text-gray-800 hover:border-gray-800 transition-all duration-300 font-medium w-full py-2 rounded-md"
              >
                {loading && (
                  <svg
                    role="status"
                    className="mr-2 w-4 text-gray-200 animate-spin dark:text-gray-600 fill-gray-700"
                    viewBox="0 0 100 101"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                      fill="currentColor"
                    />
                    <path
                      d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                      fill="currentFill"
                    />
                  </svg>
                )}
                {loading ? "Signing in..." : "Sign in"}
              </button>
            </div>
            <div className="flex items-center justify-center">
              <div className="text-sm">
                Already a user?
                <Link
                  to="/signup"
                  className="ml-1 font-medium text-indigo-600 hover:text-indigo-500"
                >
                  Sign-up
                </Link>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
