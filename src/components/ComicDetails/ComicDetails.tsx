import React, { Dispatch, SetStateAction, useEffect, useState } from "react";

// Firestore
import { db, doc, setDoc, getDoc } from "config/firebaseConfig";

// Router
import { Link, useParams, useNavigate } from "react-router-dom";

// Types
import { RandomComicType } from "types";

interface ComicDetailsPropType {
  randomComics: RandomComicType[];
  setRandomComics: Dispatch<SetStateAction<RandomComicType[]>>;
  setRender: Dispatch<SetStateAction<boolean>>;
  render: boolean;
}

function classNames(...classes: any) {
  return classes.filter(Boolean).join(" ");
}

export const ComicDetails: React.FC<ComicDetailsPropType> = ({
  randomComics,
  render,
  setRandomComics,
  setRender,
}) => {
  const [comicDetails, setComicDetails] = useState<RandomComicType>({
    alt: "",
    day: "",
    img: "",
    link: "",
    month: "",
    news: "",
    num: 0,
    safe_title: "",
    title: "",
    transcript: "",
    year: "",
    upvoters: [""],
    downvoters: [""],
  });
  const [upvoting, setUpvoting] = useState({ loading: false, id: "" });
  const [downvoting, setDownvoting] = useState({ loading: false, id: "" });
  const navigate = useNavigate();
  const [titleText, setTitleText] = useState<string>("");

  const params = useParams();

  // Handlers
  const handleUpvote = async (num: string) => {
    const docRef = doc(db, "votes", num.toString());
    setUpvoting({ loading: true, id: num });
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
      // Check if already upvoted
      const alreadyUpvoted = docSnap
        .data()
        .upvoters.find(
          (upvoter: string) => upvoter === localStorage.getItem("user")
        );

      // Check if downvoted
      const alreadyDownvoted = docSnap
        .data()
        .downvoters.find(
          (downvoter: string) => downvoter === localStorage.getItem("user")
        );

      // If Already upvoted
      if (alreadyUpvoted) {
        // * Remove from upvoters list
        let filteredUpvoters = docSnap
          .data()
          .upvoters.filter(
            (upvoter: string) => upvoter !== localStorage.getItem("user")
          );

        setDoc(doc(db, "votes", num), {
          ...docSnap.data(),
          upvoters: filteredUpvoters,
        })
          .then(() => {
            let tempComics = randomComics.filter(
              (randomComic) => randomComic?.num !== Number(num)
            );
            let tempComic = randomComics.filter(
              (randomComic) => randomComic?.num === Number(num)
            );

            setRandomComics([
              {
                ...tempComic[0],
                upvoters: filteredUpvoters,
              },
              ...tempComics,
            ]);
            setRender(!render);

            setUpvoting({ loading: false, id: "" });
          })
          .catch((err) => {
            console.error("Error removing upvote !", err);
          });
        // * Remove from upvoters list
      } else if (alreadyDownvoted) {
        console.log(
          "Downvoted ! : Removing downvote ... and adding upvote ..."
        );

        // Remove from downvoters list
        let filteredDownvoters = docSnap
          .data()
          .downvoters.filter(
            (downvoter: string) => downvoter !== localStorage.getItem("user")
          );

        setDoc(doc(db, "votes", num), {
          ...docSnap.data(),
          downvoters: filteredDownvoters,
        }).then(() => {
          let tempComics = randomComics.filter(
            (randomComic) => randomComic?.num !== Number(num)
          );
          let tempComic = randomComics.filter(
            (randomComic) => randomComic?.num === Number(num)
          );

          // * Add to upvoters list
          setDoc(doc(db, "votes", num), {
            downvoters: filteredDownvoters,
            upvoters: [
              ...docSnap.data().upvoters,
              localStorage.getItem("user"),
            ],
          })
            .then(() => {
              setRandomComics([
                {
                  ...tempComic[0],
                  downvoters: filteredDownvoters,
                  upvoters: [
                    ...docSnap.data().upvoters,
                    localStorage.getItem("user"),
                  ],
                },
                ...tempComics,
              ]);
              // * Add to downvoters list

              setRender(!render);
              setUpvoting({ loading: false, id: "" });
            })
            .catch((err) => {
              console.error("Error removing upvote !", err);
            });
        });

        // Add to downvoters list
      } else {
        // Add to upvoters list
        setDoc(doc(db, "votes", num), {
          ...docSnap.data(),
          upvoters: [...docSnap.data().upvoters, localStorage.getItem("user")],
        })
          .then(() => {
            let tempComics = randomComics.filter(
              (randomComic) => randomComic?.num !== Number(num)
            );
            let tempComic = randomComics.filter(
              (randomComic) => randomComic?.num === Number(num)
            );

            setRandomComics([
              {
                ...tempComic[0],
                upvoters: [
                  ...docSnap.data().upvoters,
                  localStorage.getItem("user"),
                ],
              },
              ...tempComics,
            ]);
            setRender(!render);
            // Add to upvoters list

            setUpvoting({ loading: false, id: "" });
          })
          .catch((err) => {
            console.error("Error upvoting !", err);
          });
      }
    }
  };

  const handleDownvote = async (num: string) => {
    const docRef = doc(db, "votes", num.toString());
    setDownvoting({ loading: true, id: num });
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
      // Check if already upvoted
      const alreadyUpvoted = docSnap
        .data()
        .upvoters.find(
          (upvoter: string) => upvoter === localStorage.getItem("user")
        );

      // Check if downvoted
      const alreadyDownvoted = docSnap
        .data()
        .downvoters.find(
          (downvoter: string) => downvoter === localStorage.getItem("user")
        );

      // If Already upvoted
      if (alreadyUpvoted) {
        // Remove from upvoters list
        let filteredUpvoters = docSnap
          .data()
          .upvoters.filter(
            (upvoter: string) => upvoter !== localStorage.getItem("user")
          );

        setDoc(doc(db, "votes", num), {
          ...docSnap.data(),
          upvoters: filteredUpvoters,
        }).then(() => {
          let tempComics = randomComics.filter(
            (randomComic) => randomComic?.num !== Number(num)
          );
          let tempComic = randomComics.filter(
            (randomComic) => randomComic?.num === Number(num)
          );

          // * Add to downvoters list
          setDoc(doc(db, "votes", num), {
            upvoters: filteredUpvoters,
            downvoters: [
              ...docSnap.data().downvoters,
              localStorage.getItem("user"),
            ],
          })
            .then(() => {
              setRandomComics([
                {
                  ...tempComic[0],
                  upvoters: filteredUpvoters,
                  downvoters: [
                    ...docSnap.data().downvoters,
                    localStorage.getItem("user"),
                  ],
                },
                ...tempComics,
              ]);
              // * Add to downvoters list

              setRender(!render);
              setDownvoting({ loading: false, id: "" });
            })
            .catch((err) => {
              console.error("Error removing upvote !", err);
            });
        });

        // Add to downvoters list
      } else if (alreadyDownvoted) {
        // * Remove from downvoters list
        let filtereDownvoters = docSnap
          .data()
          .downvoters.filter(
            (downvoter: string) => downvoter !== localStorage.getItem("user")
          );

        setDoc(doc(db, "votes", num), {
          ...docSnap.data(),
          downvoters: filtereDownvoters,
        })
          .then(() => {
            let tempComics = randomComics.filter(
              (randomComic) => randomComic?.num !== Number(num)
            );
            let tempComic = randomComics.filter(
              (randomComic) => randomComic?.num === Number(num)
            );

            setRandomComics([
              {
                ...tempComic[0],
                downvoters: filtereDownvoters,
              },
              ...tempComics,
            ]);
            setRender(!render);

            setDownvoting({ loading: false, id: "" });
          })
          .catch((err) => {
            console.error("Error removing upvote !", err);
          });
        // * Remove from downvoters list
      } else {
        // Add to downvoters list
        setDoc(doc(db, "votes", num), {
          ...docSnap.data(),
          downvoters: [
            ...docSnap.data().downvoters,
            localStorage.getItem("user"),
          ],
        })
          .then(() => {
            let tempComics = randomComics.filter(
              (randomComic) => randomComic?.num !== Number(num)
            );
            let tempComic = randomComics.filter(
              (randomComic) => randomComic?.num === Number(num)
            );

            setRandomComics([
              {
                ...tempComic[0],
                downvoters: [
                  ...docSnap.data().downvoters,
                  localStorage.getItem("user"),
                ],
              },
              ...tempComics,
            ]);
            setRender(!render);
            // Add to downvoters list

            setDownvoting({ loading: false, id: "" });
          })
          .catch((err) => {
            console.error("Error upvoting !", err);
          });
      }
    }
  };

  const handleRedirect = () => {
    navigate(`/signin`);
  };

  useEffect(() => {
    if (params?.id) {
      const filteredComic = randomComics?.filter(
        (comic) => comic?.num === Number(params?.id)
      );

      if (filteredComic[0]?.num) {
        setComicDetails(filteredComic[0]);

        if (filteredComic[0]?.transcript !== "") {
          const start = filteredComic[0]?.transcript?.indexOf("{");
          const end = filteredComic[0]?.transcript?.indexOf("}");
          const title = filteredComic[0]?.transcript
            ?.slice(start + 2, end)
            ?.split(":");
          setTitleText(title[1]?.trimStart());
        }
      }
    }
  }, [randomComics]);

  return (
    <div className="bg-white  w-full h-full">
      <div className="pt-6  h-full">
        <nav
          aria-label="Breadcrumb"
          className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8"
        >
          <ol className="flex items-center space-x-4">
            <li>
              <div className="flex items-center">
                <Link to="/" className="mr-4 text-sm font-medium text-gray-900">
                  Comics
                </Link>
                <svg
                  viewBox="0 0 6 20"
                  xmlns="http://www.w3.org/2000/svg"
                  aria-hidden="true"
                  className="h-5 w-auto text-gray-300"
                >
                  <path
                    d="M4.878 4.34H3.551L.27 16.532h1.327l3.281-12.19z"
                    fill="currentColor"
                  />
                </svg>
              </div>
            </li>
            <li className="text-sm">
              <span className="font-medium text-gray-500 hover:text-gray-600">
                {comicDetails?.safe_title}
              </span>
            </li>
          </ol>
        </nav>
        <div className="mt-4 lg:mt-6 max-w-2xl mx-auto px-4 sm:px-6 lg:max-w-7xl lg:px-8 h-full ">
          <div className="lg:grid lg:grid-cols-12 lg:auto-rows-min lg:gap-x-8 ">
            <div className="mt-8 lg:mt-0 flex flex-col items-center justify-start lg:col-start-1 lg:col-span-6 lg:row-start-1 lg:row-span-3">
              <h2 className="sr-only">Images</h2>

              <div className="grid grid-cols-1 lg:grid-cols-2 lg:grid-rows-3 lg:gap-8 ">
                <img
                  src={comicDetails?.img}
                  alt={comicDetails?.alt}
                  className="lg:col-span-2 lg:row-span-3  rounded-lg"
                />
              </div>
              {/* Voting System */}
              <div className="flex items-center justify-center w-full top-2 left-2 bg-gray-100 px-2 py-2 text-black mt-4">
                {upvoting?.loading &&
                upvoting?.id === comicDetails?.num.toString() ? (
                  <svg
                    role="status"
                    className="w-4 text-gray-200 animate-spin dark:text-gray-600 fill-gray-700"
                    viewBox="0 0 100 101"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                      fill="currentColor"
                    />
                    <path
                      d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                      fill="currentFill"
                    />
                  </svg>
                ) : (
                  <div
                    onClick={() => {
                      localStorage.getItem("user")
                        ? handleUpvote(comicDetails?.num.toString())
                        : handleRedirect();
                    }}
                    className={classNames(
                      comicDetails?.upvoters.find(
                        (upvoter: string) =>
                          upvoter === localStorage.getItem("user")
                      )
                        ? "bg-green-600 hover:bg-white"
                        : " hover:bg-green-600",
                      `group cursor-pointer border border-green-600 rounded-md flex justify-center items-center p-1 transition-all duration-150 ease-in`
                    )}
                  >
                    <svg
                      data-name="1-Arrow Up"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 32 32"
                      className={classNames(
                        comicDetails?.upvoters.find(
                          (upvoter: string) =>
                            upvoter === localStorage.getItem("user")
                        )
                          ? "fill-white group-hover:fill-green-600"
                          : "fill-green-600 group-hover:fill-white",
                        ` w-3 transition-all duration-200 ease-in-out`
                      )}
                    >
                      <path d="m26.71 10.29-10-10a1 1 0 0 0-1.41 0l-10 10 1.41 1.41L15 3.41V32h2V3.41l8.29 8.29z" />
                    </svg>
                  </div>
                )}
                <span className="mx-2 font-medium text-base pointer-events-none">
                  {comicDetails.upvoters.length -
                    comicDetails.downvoters.length}
                </span>
                {downvoting?.loading &&
                downvoting?.id === comicDetails?.num.toString() ? (
                  <svg
                    role="status"
                    className="w-4 text-gray-200 animate-spin dark:text-gray-600 fill-gray-700"
                    viewBox="0 0 100 101"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                      fill="currentColor"
                    />
                    <path
                      d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                      fill="currentFill"
                    />
                  </svg>
                ) : (
                  <div
                    onClick={() => {
                      localStorage.getItem("user")
                        ? handleDownvote(comicDetails?.num.toString())
                        : handleRedirect();
                    }}
                    className={classNames(
                      comicDetails?.downvoters.find(
                        (downvoter: string) =>
                          downvoter === localStorage.getItem("user")
                      )
                        ? "bg-red-600 hover:bg-white"
                        : " hover:bg-red-600",
                      `group cursor-pointer border border-red-600 rounded-md flex justify-center items-center p-1 transition-all duration-150 ease-in`
                    )}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 25 25"
                      className={classNames(
                        comicDetails?.downvoters.find(
                          (downvoter: string) =>
                            downvoter === localStorage.getItem("user")
                        )
                          ? "fill-white group-hover:fill-red-600"
                          : "fill-red-600 group-hover:fill-white",
                        ` w-3 transition-all duration-200 ease-in-out`
                      )}
                    >
                      <path d="m18.294 16.793-5.293 5.293V1h-1v21.086l-5.295-5.294-.707.707L12.501 24l6.5-6.5-.707-.707z" />
                    </svg>
                  </div>
                )}
              </div>
              {/* Voting System */}
            </div>

            <div className="lg:col-start-7 lg:col-span-5 mt-4 ">
              <div className="flex justify-between">
                <h1 className="text-xl font-medium text-gray-900">
                  {comicDetails?.safe_title}
                </h1>
                <p className="text-xl font-medium text-gray-900">
                  {comicDetails?.num}
                </p>
              </div>
            </div>

            <div className="mt-4 lg:col-span-5">
              {comicDetails?.transcript !== "" && (
                <div className="mt-10">
                  <h2 className="text-base font-medium text-gray-900">
                    Transcript
                  </h2>
                  <p className="mt-4 text-base text-gray-900">{titleText}</p>
                  <div className="mt-4 text-sm prose prose-sm text-gray-500">
                    {comicDetails?.transcript}
                  </div>
                </div>
              )}

              {comicDetails?.news !== "" && (
                <div className="mt-10">
                  <h2 className="text-sm font-medium text-gray-900">News</h2>
                  <div className="mt-4 prose prose-sm text-gray-500">
                    {comicDetails?.news}
                  </div>{" "}
                </div>
              )}
              <div className="mt-10">
                {comicDetails?.day !== "" && (
                  <p className="text-base font-medium text-gray-700">
                    Date : {comicDetails?.day} - {comicDetails?.month} -{" "}
                    {comicDetails?.year}
                  </p>
                )}

                {comicDetails?.link !== "" && (
                  <a
                    href={comicDetails?.link}
                    className="text-base font-medium text-gray-700"
                  >
                    Link : {comicDetails?.link}
                  </a>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
