import React, { Dispatch, SetStateAction, useEffect } from "react";

// Services
import { signOutUser } from "services/auth";

// Router
import { Link } from "react-router-dom";

interface LayoutPropType {
  children: any;
  setRender: Dispatch<SetStateAction<boolean>>;
  render: boolean;
}

export const Layout: React.FC<LayoutPropType> = ({
  children,
  render,
  setRender,
}) => {
  let user: string | null = localStorage.getItem("user");

  useEffect(() => {}, [render]);

  const handleSignOut = async () => {
    signOutUser()
      .then(() => {
        localStorage.removeItem("user");
        setRender(!render);
      })
      .catch(() => {
        console.log("Err");
      });
  };

  return (
    <div className="bg-white lg:flex lg:flex-col min-h-screen">
      <header className="bg-gray-800 w-full">
        <nav
          className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8"
          aria-label="Top"
        >
          <div className="w-full py-6 flex items-center justify-between border-b border-indigo-500 lg:border-none">
            <div className="flex items-center justify-between w-full">
              <Link to="/">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="#FFF"
                  viewBox="0 0 98 32"
                  className="w-auto h-7"
                >
                  <g>
                    <title>Tiny Logo</title>
                    <path d="M84.831 7.352l3.216 9.055.766 2.762.918-2.762 3.216-9.055H98l-8.437 23.574L84.112 32l2.25-6.385-6.584-18.263h5.053zM20.24 0c5.206.03 10.35 4.357 10.35 10.662 0 0 .032 1.602.036 3.54v.81c-.001.365-.004.735-.007 1.103l-.007.55c-.023 1.548-.077 2.989-.19 3.739-.72 4.817-4.318 8.146-9.278 8.99-4.47.874-7.12 1.38-7.977 1.549-.367.077-1.99.291-2.694.291-5.45 0-10.395-4.08-10.472-10.662v-1.081l.001-.186v-.406c.002-.709.004-1.578.008-2.472l.003-.539c.008-1.709.023-3.414.05-4.198.183-4.802 3.505-8.683 9.828-9.926L17.943.2C18.678.061 19.489 0 20.239 0zm25.392 2.44v4.912h4.594v4.45h-4.594v7.674c0 1.504 1.133 2.149 1.838 2.149.603 0 1.151-.068 1.71-.222l.28-.085 1.072 3.53c-.46.307-1.837 1.074-4.44 1.074-2.604 0-5.299-1.995-5.36-5.371-.043-2.124-.046-4.832-.007-8.124l.007-.625h-3.215v-4.45h3.215v-3.96l4.9-.952zM23.883 5.6L11.635 7.976v4.787l-4.899.95V25.65l12.248-2.378v-4.786l4.899-.951V5.599zm47.78 1.291c4.265-.03 7.499 3.335 7.65 7.577l.006.25v10.897h-4.9v-10.13c-.016-2.301-1.685-4.158-3.981-4.143-2.228.015-4.181 1.733-4.284 3.936l-.004.208v10.13h-4.9V7.351h4.44l.17 2.133c1.423-1.566 3.52-2.579 5.802-2.594zm-13.475.46v18.264h-4.9V7.352h4.9zm-39.204 3.986v7.149l-7.349 1.427v-7.15l7.349-1.426zM58.187 0v4.773l-4.9.952V.952l4.9-.952z"></path>
                  </g>
                </svg>
              </Link>
              <div className="ml-10 space-x-8 lg:block">
                {user ? (
                  <button
                    onClick={handleSignOut}
                    className="text-base font-medium text-white hover:text-indigo-50"
                  >
                    Sign Out
                  </button>
                ) : (
                  <>
                    <Link
                      to="/signin"
                      className="text-base font-medium text-white hover:text-indigo-50"
                    >
                      Sign In
                    </Link>
                    <Link
                      to="/signup"
                      className="text-base font-medium text-white hover:text-indigo-50"
                    >
                      Sign Up
                    </Link>
                  </>
                )}
              </div>
            </div>
            {/* <div className="ml-10 space-x-4">
              <a
                href="#"
                className="inline-block bg-indigo-500 py-2 px-4 border border-transparent rounded-md text-base font-medium text-white hover:bg-opacity-75"
              >
                Sign in
              </a>
              <a
                href="#"
                className="inline-block bg-white py-2 px-4 border border-transparent rounded-md text-base font-medium text-indigo-600 hover:bg-indigo-50"
              >
                Sign up
              </a>
            </div> */}
          </div>
          {/* <div className="py-4 flex flex-wrap justify-center space-x-6 lg:hidden">
            {navigation.map((link) => (
              <a
                key={link.name}
                href={link.href}
                className="text-base font-medium text-white hover:text-indigo-50"
              >
                {link.name}
              </a>
            ))}
          </div> */}
        </nav>
      </header>
      {children}
    </div>
  );
};
