// Firestore
import { db, doc, getDoc, setDoc } from "config/firebaseConfig";

// Axios
import axios from "axios";

export const getComicByNumber = async (number: number) => {
  try {
    const comic = await axios.get(
      `https://comic-strips-app-tiny.herokuapp.com/api?num=${number}`
    );

    const docRef = doc(db, "votes", number.toString());
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
      return { ...comic.data, ...docSnap.data() };
    } else {
      setDoc(doc(db, "votes", number.toString()), {
        upvoters: [],
        downvoters: [],
      })
        .then(() => {})
        .catch((err) => {
          console.error("Error adding doc !", err);
        });

      return { ...comic.data, upvoters: [], downvoters: [] };
    }
  } catch (error) {
    return error;
  }
};

export const getLastComicNumber = async () => {
  try {
    const resp = await axios.get(
      `https://comic-strips-app-tiny.herokuapp.com/api`
    );
    return resp?.data;
  } catch (error) {
    return error;
  }
};
