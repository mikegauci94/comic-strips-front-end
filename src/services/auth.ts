// Auth
import {
  auth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
} from "config/firebaseConfig";

export const signUpUser = async (
  firstName: string,
  lastName: string,
  email: string,
  password: string
) => {
  return createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Signed Up
      const user = userCredential.user;

      if (user?.uid) {
        localStorage.setItem("user", user?.uid);
      }

      return user;
    })
    .catch((error) => {
      console.log({ error });
      return error;
    });
};

export const signInUser = async (email: string, password: string) => {
  return signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Signed Up
      const user = userCredential.user;

      if (user?.uid) {
        localStorage.setItem("user", user?.uid);
      }

      return user;
    })
    .catch((error) => {
      console.log({ error });
      return error;
    });
};

export const signOutUser = async () => {
  return signOut(auth)
    .then(() => {
      return "success";
    })
    .catch((error) => {
      console.log({ error });
      return error.message;
    });
};
